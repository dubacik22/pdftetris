/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Builders;


import ConfigFactories.ThumbPageConfig;
import ConfigFactories.ThumbDocumentConfig;
import ConfigFactories.ThumbWorkConfig;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.*;
import javax.imageio.metadata.*;
import javax.imageio.stream.*;
import org.apache.pdfbox.pdmodel.*;
import org.apache.pdfbox.rendering.*;
import org.apache.pdfbox.pdmodel.common.*;
import org.w3c.dom.NodeList;
import com.googlecode.pngtastic.core.PngChunk;
import com.googlecode.pngtastic.core.PngImage;
import com.googlecode.pngtastic.core.PngOptimizer;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import resources.Dimension;

/**
 *
 * @author timo
 */
public class ThumbOutputBuilder {
    
    /** user space units per inch */
    private static final float POINTS_PER_INCH = 72;
    private Logger appLogger = null;
    private boolean verbose = false;
    
    public ThumbOutputBuilder(boolean verbose, Logger logger) {
        appLogger = logger;
        if (appLogger == null) appLogger = Logger.getLogger(ThumbOutputBuilder.class.getName());
    }
    
    public void BuildConfig(ThumbWorkConfig config) {
        boolean haveResult = false;
        for (ThumbDocumentConfig docConf : config.documents) {
            treatDocument(docConf);
            if (!haveResult) {
                config.success = docConf.success;
                haveResult = true;
            } else {
                if (!Objects.equals(config.success, docConf.success)) config.success = null;
            }
        }
    }
    
    private void treatDocument(ThumbDocumentConfig docConf) {
        PDDocument sourceDoc = null;
        boolean haveResult = false;
        try {
            tellMessage("opening: " + docConf.file.getName(), true);
            sourceDoc = PDDocument.load(docConf.file);
            PDFRenderer renderer = new PDFRenderer(sourceDoc);
            if (docConf.pages.size() > 0) {
                for (ThumbPageConfig pconf : docConf.pages) {
                    
                    treatPage(pconf, renderer, getPageDimensions(sourceDoc, pconf.pageNum), docConf.file.getParent(), docConf.file.getName());
                    if (!haveResult) {
                        docConf.success = pconf.success;
                        haveResult = true;
                    } else {
                        if (!Objects.equals(docConf.success, pconf.success)) docConf.success = null;
                    }
                }
            } else {
                for (int i = 0; i < sourceDoc.getNumberOfPages(); i++) {
                    boolean res = treatPage(renderer, i, docConf.dpi, getPageDimensions(sourceDoc, i), docConf.file.getParent(), docConf.file.getName());
                    if (!haveResult) {
                        docConf.success = res;
                        haveResult = true;
                    } else {
                        if (!Objects.equals(docConf.success, res)) docConf.success = null;
                    }
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(ThumbOutputBuilder.class.getName()).log(Level.SEVERE, null, ex);
            docConf.success = false;
        } finally {
            if (sourceDoc != null)
            {
                //wtf to odomna netbeans chce
                try {
                    sourceDoc.close();
                } catch (IOException ex) {
                    Logger.getLogger(ThumbOutputBuilder.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
    }
    
    private Dimension getPageDimensions(PDDocument sourceDoc, int pageNum) {
        PDPage page = sourceDoc.getPage(pageNum);
        PDRectangle psize = page.getMediaBox();
        Dimension size = new Dimension();
        size.setSize(psize.getWidth() / POINTS_PER_INCH, psize.getHeight() / POINTS_PER_INCH);
        return size;
    }
    
    private void treatPage(ThumbPageConfig pconf, PDFRenderer renderer, Dimension size, String path, String name) {
        pconf.success = treatPage(renderer, pconf.pageNum, pconf.dpi, size, path, name);
    }
    
    private boolean treatPage(PDFRenderer renderer, int pageNum, int dpi, Dimension size, String path, String name) {
        boolean success = false;
        try {
            DecimalFormatSymbols decimalSymbols = DecimalFormatSymbols.getInstance();
            decimalSymbols.setDecimalSeparator('p');
            DecimalFormat df = new DecimalFormat("0.00", decimalSymbols);
            String outName = String.format("%s.%d.[%sx%s].%04d.png", name, dpi, df.format(size.getWidth()), df.format(size.getHeight()), pageNum);
            String fileName = path + "/" + outName;
            tellMessage(" rendering: page" + pageNum + "(" + dpi + ") -> " + outName, false);
            BufferedImage image = renderer.renderImageWithDPI(pageNum, dpi, ImageType.RGB);
            success = writeImage(image, fileName, dpi);
        } catch (Exception ex) {
            Logger.getLogger(ThumbOutputBuilder.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (success) tellMessage(" ..ok", true);
        else tellMessage(" ..err", true);
        return success;
    }
    
    // write an image file compressed
    public boolean writeImageCompressed(BufferedImage img, String filename, int dpi) throws IOException {
 
        // convert to png
        ByteArrayOutputStream tmp = new ByteArrayOutputStream();
        ImageIO.write(img, "png", tmp);
        tmp.close();
 
        // compress
        final PngImage optimized = new PngOptimizer().optimize(new PngImage(new ByteArrayInputStream(tmp.toByteArray())), true, 9);
        final ByteArrayOutputStream optimizedBytes = new ByteArrayOutputStream();
	final long optimizedSize = optimized.writeDataOutputStream(optimizedBytes).size();
        final byte[] optimalBytes = optimizedBytes.toByteArray();
        final File exported = optimized.export(filename, optimalBytes);
        
        return true;
    }
    
    public boolean writeImage(BufferedImage image, String filename, int dpi) throws IOException
    {
        File file = new File(filename);
        FileOutputStream output = new FileOutputStream(file);
        try
        {
            String formatName = filename.substring(filename.lastIndexOf('.') + 1);
            return writeImage(image, formatName, output, dpi, 1.0f);
        }
        finally
        {
            output.close();
        }
    }
    
    public boolean writeImage(BufferedImage image, String formatName, OutputStream output, int dpi, float quality) throws IOException
    {
        ImageOutputStream imageOutput = null;
        ImageWriter writer = null;
        try
        {
            // find suitable image writer
            Iterator<ImageWriter> writers = ImageIO.getImageWritersByFormatName(formatName);
            ImageWriteParam param = null;
            IIOMetadata metadata = null;
            
            ArrayList<ImageWriter> list = new ArrayList<ImageWriter>();
            for (Iterator<ImageWriter> iter = ImageIO.getImageWritersByFormatName(formatName); iter.hasNext();) {
                list.add(iter.next());
            }
            // Loop until we get the best driver, i.e. one that supports
            // setting dpi in the standard metadata format; however we'd also 
            // accept a driver that can't, if a better one can't be found
            while (writers.hasNext())
            {
                if (writer != null)
                {
                    writer.dispose();
                }
                writer = writers.next();
                if (writer == null)
                {
                    continue;
                }
                param = writer.getDefaultWriteParam();
                metadata = writer.getDefaultImageMetadata(new ImageTypeSpecifier(image), param);
                if (metadata != null
                        && !metadata.isReadOnly()
                        && metadata.isStandardMetadataFormatSupported())
                {
                    break;
                }
            }
            if (writer == null)
            {
                appLogger.severe("No ImageWriter found for '" + formatName + "' format");
                StringBuilder sb = new StringBuilder();
                String[] writerFormatNames = ImageIO.getWriterFormatNames();
                for (String fmt : writerFormatNames)
                {
                    sb.append(fmt);
                    sb.append(' ');
                }
                appLogger.severe("Supported formats: " + sb);
                return false;
            }

            // compression
            if (param != null && param.canWriteCompressed())
            {
                param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
                param.setCompressionType(param.getCompressionTypes()[0]);
                param.setCompressionQuality(quality);
            }

            // write metadata is possible
            if (metadata != null
                    && !metadata.isReadOnly()
                    && metadata.isStandardMetadataFormatSupported())
            {
                setDPI(metadata, dpi, formatName);
            }

            // write
            imageOutput = ImageIO.createImageOutputStream(output);
            writer.setOutput(imageOutput);
            writer.write(null, new IIOImage(image, null, metadata), param);
        }
        finally
        {
            if (writer != null)
            {
                writer.dispose();
            }
            if (imageOutput != null)
            {
                imageOutput.close();
            }
        }
        return true;
    }
    // sets the DPI metadata
    private static void setDPI(IIOMetadata metadata, int dpi, String formatName) throws IIOInvalidTreeException
    {
        IIOMetadataNode root = (IIOMetadataNode) metadata.getAsTree(MetaUtil.STANDARD_METADATA_FORMAT);

        IIOMetadataNode dimension = getOrCreateChildNode(root, "Dimension");

        // PNG writer doesn't conform to the spec which is
        // "The width of a pixel, in millimeters"
        // but instead counts the pixels per millimeter
        float res = "PNG".equals(formatName.toUpperCase())
                    ? dpi / 25.4f
                    : 25.4f / dpi;

        IIOMetadataNode child;

        child = getOrCreateChildNode(dimension, "HorizontalPixelSize");
        child.setAttribute("value", Double.toString(res));

        child = getOrCreateChildNode(dimension, "VerticalPixelSize");
        child.setAttribute("value", Double.toString(res));

        metadata.mergeTree(MetaUtil.STANDARD_METADATA_FORMAT, root);
    }
    private static IIOMetadataNode getOrCreateChildNode(IIOMetadataNode parentNode, String name)
    {
        NodeList nodeList = parentNode.getElementsByTagName(name);
        if (nodeList.getLength() > 0)
        {
            return (IIOMetadataNode) nodeList.item(0);
        }
        IIOMetadataNode childNode = new IIOMetadataNode(name);
        parentNode.appendChild(childNode);
        return childNode;
    }
    
    private void tellMessage(String message, boolean newLine) {
        if (newLine) System.out.println(message);
        else System.out.print(message);
        appLogger.info(message);
    }
}
