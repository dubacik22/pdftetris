/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ConfigFactories;

import java.io.File;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author timo
 */
public class ThumbDocumentConfig {
    @XmlAttribute
    public File file;
    @XmlAttribute
    public int dpi;
    @XmlElement(name="page")
    public ArrayList<ThumbPageConfig> pages = new ArrayList<>();
    @XmlTransient
    public Boolean success;
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("file: ");
        sb.append(file.getName());
        sb.append("\n");
        if (pages.size() > 0) {
            for (ThumbPageConfig c : pages) {
                sb.append(c.toString());
            }
        } else {
            sb.append(" all: ");
            sb.append(dpi);
            sb.append("\n");
        }
        
        return sb.toString();
    }
}
