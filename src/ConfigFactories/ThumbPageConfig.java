/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ConfigFactories;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author timo
 */
public class ThumbPageConfig {
    @XmlAttribute(name="num")
    public int pageNum;
    @XmlAttribute
    public int dpi;
    @XmlTransient
    public boolean success;
    
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(" page");
        sb.append(pageNum);
        sb.append(": ");
        sb.append(dpi);
        sb.append("\n");
        
        return sb.toString();
    }
}
