/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Builders;

import ConfigFactories.TetrisWorkConfig;
import ConfigFactories.TetrisPageConfig;
import ConfigFactories.TetrisDocumentConfig;
import ConfigFactories.TetrisPictureConfig;
import java.awt.Color;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.IOException;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.pdfbox.multipdf.LayerUtility;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.graphics.form.PDFormXObject;
import org.apache.pdfbox.util.Matrix;

/**
 *
 * @author timo
 */
public class TetrisOutputBuilder {
    
    /** user space units per inch */
    private static final float POINTS_PER_INCH = 72;
    /** user space units per millimeter */
    private static final float POINTS_PER_MM = 1 / (10 * 2.54f) * POINTS_PER_INCH;
    
    private float pageh;
    private Logger appLogger = null;
    private boolean verbose = false;
    
    public static float mm2units(float mm) {
        return mm * POINTS_PER_MM;
    }
    public static float units2mm(float units) {
        return units / POINTS_PER_MM;
    }
    
    public TetrisOutputBuilder(boolean verbose, Logger logger) {
        appLogger = logger;
        if (appLogger == null) appLogger = Logger.getLogger(TetrisOutputBuilder.class.getName());
    }
    
    public void BuildConfig(TetrisWorkConfig config) {
        try {
            PDDocument target = new PDDocument();
            pageh = config.h;
            tellMessage("creating underlay pdf", true);
            PDPage page = createUnderlayingPage(config.w, config.h);
            target.addPage(page);
            LayerUtility layerUtility = new LayerUtility(target);
            PDPageContentStream contents = new PDPageContentStream(target, page, PDPageContentStream.AppendMode.OVERWRITE, false, true);
            boolean haveResult = false;
            
            for (TetrisDocumentConfig docConf : config.documents) {
                treatDocument(docConf, target, contents, layerUtility);
                if (!haveResult) {
                    config.success = docConf.success;
                    haveResult = true;
                } else {
                    if (!Objects.equals(config.success, docConf.success)) config.success = null;
                }
            }
            
            contents.close();
            target.save(config.name);
            target.close();
        } catch (IOException ex) {
            Logger.getLogger(TetrisOutputBuilder.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void treatDocument(TetrisDocumentConfig docConf, PDDocument target, PDPageContentStream contents, LayerUtility layerUtility) {
        PDDocument sourceDoc = null;
        boolean haveResult = false;
        try {
            tellMessage("opening: " + docConf.file.getName(), true);
            sourceDoc = PDDocument.load(docConf.file);
            for (TetrisPageConfig pconf : docConf.pages) {
                treatPage(pconf, sourceDoc, target, contents, layerUtility);
                if (!haveResult) {
                    docConf.success = pconf.success;
                    haveResult = true;
                } else {
                    if (!Objects.equals(docConf.success, pconf.success)) docConf.success = null;
                }
            }
            for (TetrisPictureConfig pconf : docConf.pictures) {
                treatPicture(pconf, new File(pconf.file), contents, layerUtility);
                if (!haveResult) {
                    docConf.success = pconf.success;
                    haveResult = true;
                } else {
                    if (!Objects.equals(docConf.success, pconf.success)) docConf.success = null;
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(TetrisOutputBuilder.class.getName()).log(Level.SEVERE, null, ex);
            docConf.success = false;
        } finally {
            if (sourceDoc != null)
            {
                //wtf to odomna netbeans chce
                try {
                    sourceDoc.close();
                } catch (IOException ex) {
                    Logger.getLogger(TetrisOutputBuilder.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
    }
    private void treatPage(TetrisPageConfig pconf, PDDocument sourceDoc, PDDocument target, PDPageContentStream contents, LayerUtility layerUtility) {
        try {
            if(sourceDoc.getNumberOfPages() <= pconf.pageNum) return;
            tellMessage(" appending: page" + pconf.pageNum + "(" + pconf.x + ", " + pconf.y + ", " + pconf.w + ", " + pconf.h + ")", false);
            PDFormXObject form = layerUtility.importPageAsForm(sourceDoc, pconf.pageNum);
            
            PDPage page = sourceDoc.getDocumentCatalog().getPages().get(pconf.pageNum);
            
            Matrix rotMatrix = Matrix.getRotateInstance(pconf.rotation * Math.PI, 0, 0);
            PDRectangle origPagebb = page.getBBox();
            
            if (pconf.w <= 0) pconf.w = origPagebb.getWidth();
            else pconf.w = mm2units(pconf.w);
            if (pconf.h <= 0) pconf.h = origPagebb.getHeight();
            else pconf.h = mm2units(pconf.h);
            
            PDRectangle origContoursbb = new PDRectangle(pconf.w, pconf.h);
            
            Rectangle2D newPagebb = origPagebb.transform(rotMatrix).getBounds2D();
            Rectangle2D newContbb = origContoursbb.transform(rotMatrix).getBounds2D();
            
            //corection of coordinatest due to rotation and placement space
            float yPageCor = ((float)newPagebb.getHeight() - origPagebb.getHeight()) / 2;
            float xPageCor = ((float)newPagebb.getWidth() - origPagebb.getWidth()) / 2;
            float yContoursCor = ((float)newContbb.getHeight() - origContoursbb.getHeight()) / 2;
            float xContoursCor = ((float)newContbb.getWidth() - origContoursbb.getWidth()) / 2;
            float nContoursX = mm2units(pconf.x) + origContoursbb.getWidth() / 2 + xContoursCor;
            float nContoursY = mm2units(pageh - pconf.y) - origContoursbb.getHeight() + origContoursbb.getHeight() / 2 - yContoursCor;
            
            Matrix ContoursTransformMatrix = new Matrix();
            ContoursTransformMatrix.translate(nContoursX, nContoursY); //3. shift page to its location
            ContoursTransformMatrix.rotate(-pconf.rotation * Math.PI); //2. rotate around the center
            ContoursTransformMatrix.translate(-origContoursbb.getWidth() / 2, -origContoursbb.getHeight() / 2); //1. shift page to center (set origin of rotation)
            
            Matrix PageTransformMatrix = new Matrix();
            PageTransformMatrix.translate(nContoursX, nContoursY); //3. shift page to its location
            PageTransformMatrix.rotate(-pconf.rotation * Math.PI); //2. rotate around the center
            PageTransformMatrix.translate(-origPagebb.getWidth() / 2, -origPagebb.getHeight() / 2); //1. shift page to center (set origin of rotation)
            
            contents.saveGraphicsState();
            contents.transform(ContoursTransformMatrix);
            createFillRectangle(contents, 0, 0, origContoursbb.getWidth(), origContoursbb.getHeight(), Color.WHITE);
            createFrameRectangle(contents, 0, 0, origContoursbb.getWidth(), origContoursbb.getHeight(), pconf.borderLinePattern, pconf.borderLineColor);
            contents.restoreGraphicsState();
            
            contents.saveGraphicsState();
            contents.transform(PageTransformMatrix);
            //createFillRegtangle(contents, 0, 0, origPagebb.getWidth(), origPagebb.getHeight(), Color.YELLOW);
            contents.drawForm(form);
            contents.restoreGraphicsState();

            contents.saveGraphicsState();
            contents.transform(ContoursTransformMatrix);
            createFrameRectangle(contents, 0, 0, origContoursbb.getWidth(), origContoursbb.getHeight(), pconf.borderLinePattern, pconf.borderLineColor);
            contents.restoreGraphicsState();
            
            pconf.success = true;
        } catch (IOException ex) {
            Logger.getLogger(TetrisOutputBuilder.class.getName()).log(Level.SEVERE, null, ex);
            pconf.success = false;
        }
        if (pconf.success) tellMessage(" ..ok", true);
        else tellMessage(" ..err", true);
    }
    private void treatPicture(TetrisPictureConfig pconf, File pictureFile, PDPageContentStream contents, LayerUtility layerUtility) {
        
    }
    
    private void createFillRectangle(PDPageContentStream contents, float x, float y, float w, float h, Color color) throws IOException {
        contents.addRect(x, y, w, h);
        contents.setNonStrokingColor(color);
        contents.fill();
    }
    
    private void createFrameRectangle(PDPageContentStream contents, float x, float y, float w, float h, float[] pattern, Color color) throws IOException {
        contents.addRect(x, y, w, h);
        contents.setLineWidth(0.1f);
        contents.setLineDashPattern(pattern , 0);
        contents.setStrokingColor(color);
        contents.stroke();
    }
    
    private PDPage createUnderlayingPage(float width_mm, float height_mm){
        PDRectangle pageSize = new PDRectangle(mm2units(width_mm), mm2units(height_mm));
        return new PDPage(pageSize);
    }
    
    private void tellMessage(String message, boolean newLine) {
        if (newLine) System.out.println(message);
        else System.out.print(message);
        appLogger.info(message);
    }
}
