/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ConfigFactories;

import java.awt.Color;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author timo
 */
public class TetrisPictureConfig {
    public String file;
    @XmlAttribute
    public float x;
    @XmlAttribute
    public float y;
    @XmlAttribute
    public float w = 0;
    @XmlAttribute
    public float h = 0;
    @XmlAttribute(name="rot")
    public float rotation = 0;
    @XmlAttribute(name="borderLineStyle")
    public String borderLineStyleString;
    @XmlAttribute(name="borderLineColor")
    public String borderLineColorString;
    @XmlAttribute(name="resample")
    public int dpi;
    
    
    @XmlTransient
    public float[] borderLinePattern = null;
    @XmlTransient
    public Color borderLineColor = null;
    
    @XmlTransient
    public float scale = 1;
    @XmlTransient
    public boolean success;
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("  picture \"");
        sb.append(file);
        sb.append("\"(");
        sb.append(x);
        sb.append(", ");
        sb.append(y);
        sb.append(") r:");
        sb.append(rotation);
        sb.append(" s:");
        sb.append(scale);
        sb.append("\n");
        
        return sb.toString();
    }
}
