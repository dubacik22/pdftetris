/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ConfigFactories;

import java.io.StringReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

/**
 *
 * @author timo
 */
public class TetrisXmlStringConfigFactory extends TetrisXmlConfigFactory {
    String xmlString;
    
    public TetrisXmlStringConfigFactory(String xmlString, Logger logger) {
        super(logger);
        this.xmlString = xmlString;
    }
    
    @Override
    public TetrisWorkConfig BuildConfig() {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(TetrisWorkConfig.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            StringReader xmlStringReader = new StringReader(xmlString);
            TetrisWorkConfig wc = (TetrisWorkConfig) jaxbUnmarshaller.unmarshal(xmlStringReader);
            
            for (TetrisDocumentConfig dconf : wc.documents) {
                initDocumentConfig(dconf);
            }
            
            return wc;
        } catch (JAXBException ex) {
            Logger.getLogger(TetrisXmlStringConfigFactory.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
}
