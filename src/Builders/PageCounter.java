/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Builders;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import resources.ArgsFetcher;

/**
 *
 * @author timo
 */
public class PageCounter {
    File pdfFile;
    private Logger appLogger = null;
    
    public PageCounter(String pdfFileName, Logger logger) {
        appLogger = logger;
        pdfFile = new File(pdfFileName);
    }
    
    public int fetchCount() {
        try {
            if (!pdfFile.exists()) {
                tellMessage("file does not exist " + pdfFile.getPath());
                return -1;
            }
            if (!pdfFile.canRead()) {
                tellMessage("cannot read file " + pdfFile.getPath());
                return -1;
            }
            
            PDDocument sourceDoc = PDDocument.load(pdfFile);
            int ret = sourceDoc.getNumberOfPages();
            tellMessage("Count of pages for \"" + pdfFile.getName() + "\": " + ret + ".");
            sourceDoc.close();
            return ret;
            
        } catch (IOException ex) {
            Logger.getLogger(PageCounter.class.getName()).log(Level.SEVERE, null, ex);
            return -1;
        }
    }
    
    private void tellMessage(String message) {
        System.out.println(message);
        appLogger.info(message);
    }
}
