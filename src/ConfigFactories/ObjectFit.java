/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ConfigFactories;

/**
 *
 * @author timo
 */
public enum ObjectFit {
    Contain,
    Fill,
    None,
    Cover
}
