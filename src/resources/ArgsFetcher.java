/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resources;

import java.util.List;
import org.apache.commons.validator.routines.FloatValidator;
import org.apache.commons.validator.routines.IntegerValidator;

/**
 *
 * @author timo
 */
public class ArgsFetcher {
    
    IntegerValidator iv = new IntegerValidator();
    FloatValidator fv = new FloatValidator();
    private List<String> args;
    private int i = 0;
    
    public ArgsFetcher(List<String> args) {
        this.args = args;
    }
    public ArgsFetcher(List<String> args, int i) {
        this.args = args;
        this.i = i;
    }
    
    public int getIndex(){
        return i;
    }
    
    public String FetchString() {
        if (canFetch()) return args.get(i);
        return null;
    }
    public String FetchStringAndMove() {
        if (canFetch()) return args.get(i++);
        return null;
    }
    
    public int Count() {
        if (!canFetch()) return 0;
        return args.size() - i - 1;
    }
    
    private boolean canFetch() {
        return i < args.size();
    }
    public boolean haveNext() {
        return i < args.size() - 1;
    }
    
    public void moveBack() {
        if (i > 0) i--;
    }
    
    public boolean moveNext() {
        if (haveNext()) {
            i++;
            return true;
        }
        return false;
    }
    
    public Integer FetchInt() {
        return iv.validate(FetchString());
    }
    public Integer FetchIntAndMove() {
        return iv.validate(FetchStringAndMove());
    }
    public Float FetchFloat() {
        return fv.validate(FetchString());
    }
    public Float FetchFloatAndMove() {
        return fv.validate(FetchStringAndMove());
    }
}
