/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ConfigFactories;

import java.io.File;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author timo
 */
public class TetrisDocumentConfig {
    @XmlAttribute
    public File file;
    @XmlElement(name="page")
    public ArrayList<TetrisPageConfig> pages = new ArrayList<>();
    @XmlElement(name="picture")
    public ArrayList<TetrisPictureConfig> pictures = new ArrayList<>();
    @XmlTransient
    public Boolean success;
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(" file: ");
        sb.append(file.getName());
        sb.append("\n");
        
        for (TetrisPageConfig c : pages) {
            sb.append(c.toString());
        }
        
        return sb.toString();
    }
}
