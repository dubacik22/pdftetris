/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ConfigFactories;

import ConfigFactories.TetrisDocumentConfig;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author timo
 */
@XmlRootElement
public class TetrisWorkConfig {
    @XmlAttribute
    public float w;
    @XmlAttribute
    public float h;
    @XmlAttribute
    public String name;
    
    @XmlTransient
    public Boolean success;
    
    @XmlElement(name="document")
    public ArrayList<TetrisDocumentConfig> documents = new ArrayList<>();
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("output: ");
        sb.append(name);
        sb.append("\n");
        
        if (documents.size() > 0) {
            for (TetrisDocumentConfig c : documents) {
                sb.append(c.toString());
            }
        } else {
            sb.append(" no input files configured");
        }
        return sb.toString();
    }
}
