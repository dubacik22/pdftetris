/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ConfigFactories;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

/**
 *
 * @author timo
 */
public class TetrisXmlFileConfigFactory extends TetrisXmlConfigFactory {
    File xmlFile;
    
    public TetrisXmlFileConfigFactory(String xmlFileName, Logger logger) {
        super(logger);
        xmlFile = new File(xmlFileName);
    }
    
    @Override
    public TetrisWorkConfig BuildConfig() {
        try {
            if (!xmlFile.exists()) {
                tellMessage("file does not exist: " + xmlFile.getPath());
                return null;
            }
            JAXBContext jaxbContext = JAXBContext.newInstance(TetrisWorkConfig.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            TetrisWorkConfig wc = (TetrisWorkConfig) jaxbUnmarshaller.unmarshal(xmlFile);
            
            for (TetrisDocumentConfig dconf : wc.documents) {
                initDocumentConfig(dconf);
            }
            
            return wc;
        } catch (JAXBException ex) {
            Logger.getLogger(TetrisXmlFileConfigFactory.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
}
