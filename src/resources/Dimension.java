/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resources;

import java.awt.geom.Dimension2D;

/**
 *
 * @author timo
 */
public class Dimension extends Dimension2D {

    private double w;
    private double h;
    
    public Dimension() {
        w = Double.NaN;
        h = Double.NaN;
    }
    public Dimension(double width, double height) {
        w = width;
        h = height;
    }
    
    @Override
    public double getWidth() {
        return w;
    }

    @Override
    public double getHeight() {
        return h;
    }

    @Override
    public void setSize(double width, double height) {
        w = width;
        h = height;
    }
    
}
