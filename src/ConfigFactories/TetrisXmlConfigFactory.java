/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ConfigFactories;

import Builders.TetrisOutputBuilder;
import java.awt.Color;
import java.lang.reflect.Field;
import java.util.LinkedList;
import java.util.logging.Logger;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.FloatValidator;
import org.apache.commons.validator.routines.IntegerValidator;

/**
 *
 * @author timo
 */
public abstract class TetrisXmlConfigFactory implements iTetrisConfigFactory {
    private Logger appLogger = null;
    
    public TetrisXmlConfigFactory(Logger logger) {
        appLogger = logger;
    }
    
    private Color initPageBorderColor(String value) {
        Color defColor = Color.BLACK;
        if (!StringUtils.isEmpty(value)) {
            if(value.contains(",")) {
                String[] rgb = value.split(",");
                if (rgb.length != 3) return defColor;
                Color ret = parseRGBColor(rgb[0], rgb[1], rgb[2]);
                if (ret == null) return defColor;
                return ret;
            } else {
                Color ret = parseColor(value);
                if (ret == null) return defColor;
                return ret;
            }
        } else {
            return defColor;
        }
    }
    private Color parseColor(String name) {
        Color color;
        try {
            Field field = Class.forName("java.awt.Color").getField(name);
            color = (Color)field.get(null);
        } catch (Exception e) {
            color = null; // Not defined
        }
        return color;
    }
    private Color parseRGBColor(String r, String g, String b) {
        IntegerValidator iv = new IntegerValidator();
        Integer ir = iv.validate(r);
        if (ir == null) return null;
        Integer ig = iv.validate(g);
        if (ig == null) return null;
        Integer ib = iv.validate(b);
        if (ib == null) return null;
        
        return new Color(ir, ig, ib);
    }
    private Color parseHSBColor(String h, String s, String b) {
        FloatValidator fv = new FloatValidator();
        Float fh = fv.validate(h);
        if (fh == null) return null;
        Float fs = fv.validate(s);
        if (fs == null) return null;
        Float fb = fv.validate(b);
        if (fb == null) return null;
        
        return Color.getHSBColor(fh, fs, fb);
    }
    
    private float[] initPageBorderPattern(String value) {
        float[] defPattern = new float[] {20, 50, 2, 50};
        if (!StringUtils.isEmpty(value)) {
            if(value.contains(",")) {
                String[] pattern = value.split(",");
                if ((pattern.length % 2) != 0) return defPattern;
                float[] ret = parseFloatArray(pattern);
                if (ret == null) return defPattern;
                mm2units(ret);
                return ret;
            } else {
                switch(value.toLowerCase()) {
                    case "solid":
                    case "-":
                        return new float[] {10, 0};
                    case "dotted":
                    case ":":
                        return new float[] {2, 50};
                    case "dashed":
                    case "--":
                        return new float[] {20, 50};
                    case "dash-dot":
                    case "-.":
                    default:
                        return new float[] {20, 50, 2, 50};
                }
            }
        } else {
            return defPattern;
        }
    }
    private float[] parseFloatArray(String[] value) {
        if (value == null) return null;
        if (value.length <= 0) return null;
        LinkedList<Float> floatList = new LinkedList<>();
        FloatValidator fv = new FloatValidator();
        for (String v : value) {
            Float f = fv.validate(v);
            if (f == null) return null;
            floatList.addLast(f);
        }
        float[] ret = new float[floatList.size()];
        int i = 0;
        for (Float f : floatList) {
            ret[i++] = f;
        }
        return ret;
    }
    private void mm2units(float[] arr) {
        for (int i = 0; i < arr.length; i++) {
            arr[i] = TetrisOutputBuilder.mm2units(arr[i]);
        }
    }
    
    private void initPageConfig(TetrisPageConfig pconf) {
        pconf.borderLineColor = initPageBorderColor(pconf.borderLineColorString);
        pconf.borderLinePattern = initPageBorderPattern(pconf.borderLineStyleString);
    }
    protected void initDocumentConfig(TetrisDocumentConfig dconf) {
        for (TetrisPageConfig pconf : dconf.pages) {
            initPageConfig(pconf);
        }
    }
    
    protected void tellMessage(String message) {
        System.out.println(message);
        appLogger.info(message);
    }
}
