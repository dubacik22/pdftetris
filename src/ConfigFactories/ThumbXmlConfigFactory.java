/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ConfigFactories;

import java.util.logging.Logger;

/**
 *
 * @author timo
 */
public abstract class ThumbXmlConfigFactory implements iThumbConfigFactory {
    private Logger appLogger = null;
    
    public ThumbXmlConfigFactory(Logger logger) {
        appLogger = logger;
    }
    
    protected void tellMessage(String message) {
        System.out.println(message);
        appLogger.info(message);
    }
}
