/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pdftetris;

import Builders.PageCounter;
import Builders.TetrisOutputBuilder;
import Builders.ThumbOutputBuilder;
import ConfigFactories.TetrisWorkConfig;
import ConfigFactories.TetrisXmlFileConfigFactory;
import ConfigFactories.TetrisXmlStringConfigFactory;
import ConfigFactories.ThumbWorkConfig;
import ConfigFactories.ThumbXmlFileConfigFactory;
import ConfigFactories.ThumbXmlStringConfigFactory;
import ConfigFactories.iTetrisConfigFactory;
import ConfigFactories.iThumbConfigFactory;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 *
 * @author timo
 */
public class PdfTetris {

    private static Logger appLogger;
    private static List<String> sargs = new ArrayList<>();
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        for (String arg : args) sargs.add(arg);
        
        if (sargs.size()< 1) {
            printReadme();
            return;
        }
        if (haveArg("--version")) {
            System.out.println("version 2.0.0");
            return;
        }
        configureLogger(getArg("-logPath"));
        if (haveArg("demo")) sargs = fakeXmlFileTetrisArgs();
        
        try
        {
            switch (sargs.get(0).toLowerCase()) {    
                case "-pagecount":
                    doPageCounter(1);
                    break;
                case "-tetrisxmlstr":
                    doXmlTetris4String(1);
                    break;
                case "-tetrisxmlfile":
                    doXmlTetris4File(1);
                    break;
                case "-thumbxmlstr":
                    doXmlThumb4String(1);
                    break;
                case "-thumbxmlfile":
                    doXmlThumb4File(1);
                    break;
                default:
                    printReadme();
                    return;
            }
            return;
        }
        catch(Exception ex)            
        {
            Logger.getLogger(PdfTetris.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private static String getArg(String arg) {
        int i = sargs.indexOf(arg);
        if (i < 0) return null;
        if (i == sargs.size() - 1) return "";
        return sargs.get(i + 1);
    }
    private static boolean haveArg(String arg) {
        return sargs.contains(arg);
    }
    
    private static void printReadme() {
        File rf = new File("README.TXT");
        System.out.println("Readme:");
        if (!rf.exists()) {
            return;
        }
        try (BufferedReader br = new BufferedReader(new FileReader("README.TXT"))) {
            String line = null;
            while ((line = br.readLine()) != null) {
                System.out.println(line);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(PdfTetris.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(PdfTetris.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private static void doXmlTetris4File(int i) {
        tellMessage("reading config", true);
        doTetris(new TetrisXmlFileConfigFactory(sargs.get(i), appLogger));
    }
    private static void doXmlTetris4String(int i) {
        tellMessage("reading config", true);
        doTetris(new TetrisXmlStringConfigFactory(sargs.get(i), appLogger));
    }
    private static void doTetris(iTetrisConfigFactory cf) {
        if (cf == null) return;
        TetrisWorkConfig config = cf.BuildConfig();
        
        tellMessage("will create", true);
        tellMessage(config.toString(), true);

        tellMessage("creating", true);
        TetrisOutputBuilder ob = new TetrisOutputBuilder(true, appLogger);
        ob.BuildConfig(config);
    }
    
    private static void doXmlThumb4File(int i) {
        tellMessage("reading config", true);
        doThumb(new ThumbXmlFileConfigFactory(sargs.get(i), appLogger));
    }
    private static void doXmlThumb4String(int i) {
        tellMessage("reading config", true);
        doThumb(new ThumbXmlStringConfigFactory(sargs.get(i), appLogger));
    }
    private static void doThumb(iThumbConfigFactory cf) {
        if (cf == null) return;
        ThumbWorkConfig config = cf.BuildConfig();
        
        tellMessage("will create", true);
        tellMessage(config.toString(), true);
        
        tellMessage("creating", true);
        ThumbOutputBuilder ob = new ThumbOutputBuilder(true, appLogger);
        ob.BuildConfig(config);
    }
    
    private static void doPageCounter(int i) {
        PageCounter counter = new PageCounter(sargs.get(i), appLogger);
        tellMessage("will create", true);
        System.out.print(counter.fetchCount());
    }
    
    private static void configureLogger(String path) {
        try {
            if (path == null) path = "";
            String timeStamp = new SimpleDateFormat("yyyy-MM-dd.HH.mm.ss").format(new Date());
            String fileName = path + "PdfTetris_" + timeStamp + ".log";
            if (new File(path).exists()) {
                FileHandler fileHandler = new FileHandler(fileName, false);
                fileHandler.setLevel(Level.CONFIG);
                fileHandler.setFormatter(new SimpleFormatter());

                Logger gLogger= Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
                for(Handler handler : gLogger.getHandlers()) {
                    gLogger.removeHandler(handler);
                }
                gLogger.addHandler(fileHandler);

                gLogger= Logger.getLogger("");
                for(Handler handler : gLogger.getHandlers()) {
                    gLogger.removeHandler(handler);
                }
                gLogger.addHandler(fileHandler);
            } else {
                Logger.getLogger("app").log(Level.SEVERE, "log path " + path + " whole file name (" + fileName + ") does not exist");
            }
            appLogger = Logger.getLogger("app");
            
        } catch (IOException ex) {
            appLogger.log(Level.SEVERE, null, ex);
        } catch (SecurityException ex) {
            appLogger.log(Level.SEVERE, null, ex);
        }
    }
    
    private static void tellMessage(String message, boolean newLine) {
        if (newLine) System.out.println(message);
        else System.out.print(message);
        appLogger.info(message);
    }
    
    private static List<String> fakeXmlFileTetrisArgs() {
        ArrayList<String> al = new ArrayList<>();
        al.add("-tetrisxmlfile");
        al.add("tetrisXmlConfig.xml");
        
        return al;
    }
    private static List<String> fakeXmlFileThumbArgs() {
        ArrayList<String> al = new ArrayList<>();
        al.add("-thumbXmlFile");
        al.add("thumbXmlConfig.xml");
        
        return al;
    }
    
    private static List<String> fakePageCountArgs() {
        ArrayList<String> al = new ArrayList<>();
        al.add("-pageCount");
        al.add("ff/input1.pdf");
        
        return al;
    }
}
