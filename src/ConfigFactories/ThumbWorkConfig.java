/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ConfigFactories;

import java.util.ArrayList;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author timo
 */
@XmlRootElement
public class ThumbWorkConfig {
    //public String name;
    @XmlTransient
    public Boolean success;
    @XmlElement(name="document")
    public ArrayList<ThumbDocumentConfig> documents = new ArrayList<>();
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        for (ThumbDocumentConfig c : documents) {
            sb.append(c.toString());
        }
        return sb.toString();
    }
}
